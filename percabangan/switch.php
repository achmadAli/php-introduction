<?php 

$kelas = 10;

echo "<h1> contoh kalau pakai if-else </h1>";
if($kelas == 12){
  echo "ayo siap-siap cari kerja"; 
} else if($kelas == 11){
  echo "udah punya skill apa ?"; 
} else if($kelas == 10){
  echo "AYO SEMANGAT BELAJARNYA!!";
} else {
  echo "Salah Server gan";
}


echo "<h1> contoh kalau pakai switch-case </h1>";
switch ($kelas) {
  case 12:
    echo "ayo siap-siap cari kerja"; 
    break;
  case 11:
    echo "udah punya skill apa ?"; 
    break;
  case 10:
    echo "AYO SEMANGAT BELAJARNYA!!";
    break;
  default:
    echo "Salah Server gan";
    break;
}