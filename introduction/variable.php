<?php 

  $string = "Ini adalah variabel dengan tipe data string";
  $other_string = 'Ini adalah variabel dengan tipe data string';
  $integer = 10;
  $float = 1.5;

  $boolean = true;
  $other_boolean = false;
  // $boolean = 1;
  // $other_boolean = 0;

  $array = ['nilai pertama, index 0', 'nilai kedua, index 1'];

  echo "
  ini adalah variabel string :  $string <br> <br> 
  ini adalah variabel string yang lain :  $other_string <br> <br> 
  ini adalah variabel integer :  $integer <br> <br> 
  ini adalah variabel float :  $float <br> <br> 
  ini adalah variabel boolean :  $boolean , jika muncul 1 berarti nilainya true <br> <br> 
  ini adalah variabel boolean :  $other_boolean , jika tidak muncul berarti nilainya false <br> <br> 
  ini adalah variabel array :  $array , akan muncul tulisan array karena harus didefinisikan dengan jelas nilai yang mana yang mau ditampilkan  <br> <br> 
  ini adalah variabel array index 0 :  $array[0] <br> <br> 
  ini adalah variabel array index 1 :  $array[1] <br> <br> 
  ";

?>